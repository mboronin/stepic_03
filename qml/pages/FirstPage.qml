import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.Portrait

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("Show Page 2")
                onClicked: pageStack.push(Qt.resolvedUrl("SecondPage.qml"))
            }
            MenuItem {
                text: qsTr("Show Page 3")
                onClicked: pageStack.push(Qt.resolvedUrl("ThirdPage.qml"))
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("UI Template")
            }
            Button{
                anchors.horizontalCenter: parent.horizontalCenter
                id: button
                text: "Кнопка"
                onPressed: label.text = "Нажата"
                onReleased: label.text = "Отпущена"
            }
            Label{
                anchors.horizontalCenter: parent.horizontalCenter
                id: label
                text: "Отпущена"
            }
            ValueButton{
                anchors.horizontalCenter: parent.horizontalCenter
                property int counter: 0
                value: counter.toString()
                onPressed: counter += 1
                label: "Счетчик"
                description: "Считает количество нажатий"
            }

        }
    }
}
