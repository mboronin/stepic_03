import QtQuick 2.0
import Sailfish.Silica 1.0

Page {

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: col.height
        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("Show Page 1")
                onClicked: pageStack.push(Qt.resolvedUrl("FirstPage.qml"))
            }
            MenuItem {
                text: qsTr("Show Page 3")
                onClicked: pageStack.push(Qt.resolvedUrl("ThirdPage.qml"))
            }
        }
        id: page

        // The effective value will be restricted by ApplicationWindow.allowedOrientations

        Column{
            id: col
            DatePicker{
                anchors.horizontalCenter: parent.horizontalCenter
                onDateChanged: console.log(date.toDateString())
            }
            TimePicker {
                hour: 1
                minute: 5
                anchors.horizontalCenter: parent.horizontalCenter
                onTimeChanged: console.log(hour, minute)
            }
        }
    }

}
