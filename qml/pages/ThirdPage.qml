import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    SilicaFlickable{
        anchors.fill: parent
        contentHeight: column.height
        PullDownMenu {
            MenuItem {
                text: qsTr("Show Page 1")
                onClicked: pageStack.push(Qt.resolvedUrl("FirstPage.qml"))
            }
            MenuItem {
                text: qsTr("Show Page 2")
                onClicked: pageStack.push(Qt.resolvedUrl("SecondPage.qml"))
            }
    }

    Column{
        spacing: Theme.paddingLarge

        width: parent.width
        id: column
        TextSwitch {
            anchors.horizontalCenter: parent.horizontalCenter
             id: activationSwitch
             text: "Выключен"
             onClicked: {
                 checked ? text = "Включен" : text = "Выключен"
             }
         }
        ComboBox{

            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            label: "Select language"
            menu : ContextMenu{
                MenuItem { text: "C++"}
                MenuItem { text: "Java"}
            }

        }
        Slider{
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            value: 50
            stepSize: 1
            minimumValue: 0
            maximumValue: 100
            valueText: value
        }

    }
    }
}
